﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WarwickTest
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
               name: "CustomerList",
               url: "customers",
               defaults: new { controller = "Customer", action = "CustomerList", id = UrlParameter.Optional }
           );
            routes.MapRoute(
               name: "CustomerView",
               url: "customers/{CustomerID}",
               defaults: new { controller = "Customer", action = "CustomerView", id = UrlParameter.Optional }
           );
            routes.MapRoute(
              name: "ServiceCreate",
              url: "services/create/{VehicleID}",
              defaults: new { controller = "Service", action = "ServiceCreate", id = UrlParameter.Optional }
          );
            routes.MapRoute(
             name: "ServiceEdit",
             url: "services/edit/{ServiceID}",
             defaults: new { controller = "Service", action = "ServiceEdit", id = UrlParameter.Optional }
         );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Service", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}