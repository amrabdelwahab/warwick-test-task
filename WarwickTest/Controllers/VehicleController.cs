﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WarwickTest.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
namespace WarwickTest.Controllers
{
    public class VehicleController : Controller
    {
        //
        // GET: /Vehicle/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserVehicleListAjax(int id, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetVehicles(id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        private static IEnumerable<ViewModels.VehicleVM> GetVehicles(int id)
        {
            var db = new VehicleServicingEntities();

            return db.Vehicles.Where(sp => sp.CustomerID == id).Select(sp => new ViewModels.VehicleVM
            {
                ID = sp.ID,
                Make = sp.Make,
                Model = sp.Model,
                Variation = sp.Variation,
                FuelType = sp.FuelType,
                Year = sp.Year,
                Colour = sp.Colour

            });
        }
       
    }
}
