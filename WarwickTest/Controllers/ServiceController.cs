﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WarwickTest.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
namespace WarwickTest.Controllers
{
    public class ServiceController : Controller
    {
        //
        // GET: /Service/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ServiceCreate(ViewModels.ServiceVM aService, int VehicleID)
        {

            using (var db = new VehicleServicingEntities())
            {
                VehicleService rService = new VehicleService
                {
                    MechanicName = aService.MechanicName,
                    Price = aService.Price,
                    Notes = aService.Notes,
                    VehicleID = VehicleID,
                    ServiceDate = DateTime.UtcNow

                };

                db.VehicleServices.Add(rService);
                db.SaveChanges();

                return new EmptyResult();
            }

        }
        public ActionResult VehicleServicesListAjax(int VehicleID, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetVehicleServices(VehicleID).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        private static IEnumerable<ViewModels.ServiceVM> GetVehicleServices(int id)
        {
            var db = new VehicleServicingEntities();

            return db.VehicleServices.Where(sp => sp.VehicleID == id).Select(sp => new ViewModels.ServiceVM
            {
                VehicleID = id,
                ID = sp.ID,
                MechanicName = sp.MechanicName,
                Notes = sp.Notes,
                Price = sp.Price

            });
        }
    
        [HttpPost]
        public ActionResult ServiceEdit(int ServiceID, ViewModels.ServiceVM aService)
        {
            VehicleServicingEntities db = new VehicleServicingEntities();

            var rService = db.VehicleServices.Find(ServiceID);
            rService.MechanicName = aService.MechanicName;
            rService.Price = aService.Price;
            rService.Notes = aService.Notes;
            db.SaveChanges();
            return new EmptyResult();
        }


    }

}
