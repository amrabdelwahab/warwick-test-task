﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WarwickTest.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace WarwickTest.Controllers
{
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CustomerView(int CustomerID)
        {
            using (var db = new VehicleServicingEntities())
            {
                var rCustomer = db.Customers.Find(CustomerID);
                var model = new ViewModels.CustomerVM
                {
                    ID = rCustomer.ID,
                    Notes = rCustomer.Notes,
                    Surname = rCustomer.Surname,
                    Forename = rCustomer.Forename,
                    Address = rCustomer.Address,
                    Postcode = rCustomer.Postcode
                };
                return View("details", model);
            }
        }
           public ActionResult CustomerList()
        {
            using (var db = new VehicleServicingEntities())
            {


                var Customers = (from b in db.Customers
                                 select new ViewModels.CustomerVM
                                 {
                                     ID = b.ID,
                                     Name = b.Forename + " " + b.Surname,
                                     Postcode = b.Postcode
                                 }
                              ).ToList();

                return View("List", Customers);



            }
        }
        public ActionResult CustomerListAjax([DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetCustomers().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        private static IEnumerable<ViewModels.CustomerVM> GetCustomers()
        {
            var db = new VehicleServicingEntities();

            return db.Customers.Select(sp => new ViewModels.CustomerVM
            {
                ID = sp.ID,
                Name = sp.Forename + " " + sp.Surname,
                Postcode = sp.Postcode
            });
        }

    }
}
