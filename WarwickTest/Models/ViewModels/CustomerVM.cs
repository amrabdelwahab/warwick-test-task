﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace WarwickTest.ViewModels
{
    public class CustomerVM
    {

        public CustomerVM()
        {
            Name = string.Empty;
            Forename = string.Empty;
            Surname = string.Empty;
            Address = string.Empty;
            Postcode = string.Empty;
            Notes = string.Empty;
            
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
   
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string Notes { get; set; }

        public IList<VehicleVM> Vehicles { get; set; }
    }
}