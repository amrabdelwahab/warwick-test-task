﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace WarwickTest.ViewModels
{
    public class VehicleVM
    {

        public VehicleVM()
        {
            Make = string.Empty;
            Model = string.Empty;
            Variation = string.Empty;
            FuelType = string.Empty;
            Year=string.Empty;
            Colour = string.Empty;
        }

        public int ID { get; set; }
        public int CustomerID { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variation { get; set; }
        public string FuelType { get; set; }
        public string Year { get; set; }
        public string Colour { get; set; }

        public IList<ServiceVM> Services { get; set; }
    }
}