﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace WarwickTest.ViewModels
{
    public class ServiceVM
    {

        public ServiceVM()
        {
            MechanicName = string.Empty;
            Notes = string.Empty;
            Price = 0;
        }

        public int ID { get; set; }
        public int VehicleID { get; set; }
        public string MechanicName { get; set; }
        public string Notes { get; set; }
        public decimal Price { get; set; }

        
    }
}